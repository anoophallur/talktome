Element.prototype.remove = function() {
    this.parentElement.removeChild(this);
}
NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
    for(var i = 0, len = this.length; i < len; i++) {
        if(this[i] && this[i].parentElement) {
            this[i].parentElement.removeChild(this[i]);
        }
    }
}

var e = document.getElementsByClassName("yt-uix-button-content")[16];
e.click();
var f = document.getElementsByClassName('yt-ui-menu-item-icon yt-uix-button-icon-action-panel-transcript yt-sprite')[0];
f.click();
var g = document.getElementById('transcript-scrollbox');

var times, texts;
var no_of_captions;
setTimeout(function(){

	var captionlines = g.getElementsByClassName('caption-line');

	texts = [];
	times = [];

	no_of_captions = captionlines.length;

	for (var i = 0; i < no_of_captions; i++) {
	    var captionline = captionlines[i];
	    var text = captionline.getElementsByClassName('caption-line-text')[0].innerText;
	    var time = captionline.getElementsByClassName('caption-line-time')[0].innerText;
	    texts.push(text);
	    times.push(time);
	}

	// console.log(texts);	
	// console.log(times);	
}, 1000);


var watchheader = document.getElementById("page");
var speechbox = document.createElement("div");
str = '<div id="div_start" style="float: right;"> \
    <button id="start_button" onclick="startButton(event)" style="display: inline-block;  border: 0; \
    background-color: transparent; padding: 0;"> \
        <img alt="Start"\
         id="start_img" \
         src="https://raw.githubusercontent.com/anooprh/PyOhio-Prsesentation/master/images/mic.gif"> \
    </button> \
</div> \
<div id="results" style="font-size: 2em; font-weight: bold; border: 1px solid #ddd; border-radius: 1px; \
    padding: 10px; text-align: left; min-height: 30px; background-color: white; color: #000000;"> \
     <button><img src="http://i.stack.imgur.com/vDH0q.png" width="30px" height="30px"></button> \
     <span class="final" id="final_span" style="padding-right: 3px;"></span> \
     <span class="interim" id="interim_span" style="color: gray; margin-bottom: 10px;"></span> \
</div>'; 

speechbox.innerHTML = str;
watchheader.insertBefore(speechbox,document.getElementById("player"));

var final_transcript = '';
var recognizing = false;
var ignore_onend;
var start_timestamp;
if (!('webkitSpeechRecognition' in window)) {
    upgrade();
} else {

    start_button.style.display = 'inline-block';
    var recognition = new webkitSpeechRecognition();
    recognition.continuous = false;
    recognition.interimResults = false;

    recognition.onstart = function () {
        recognizing = true;
        start_img.src = 'https://raw.githubusercontent.com/anooprh/PyOhio-Prsesentation/master/images/mic-animate.gif';
    };

    recognition.onerror = function (event) {
        if (event.error == 'no-speech') {
            start_img.src = 'https://raw.githubusercontent.com/anooprh/PyOhio-Prsesentation/master/images/mic.gif';
            ignore_onend = true;
        }
        if (event.error == 'audio-capture') {
            start_img.src = 'https://raw.githubusercontent.com/anooprh/PyOhio-Prsesentation/master/images/mic.gif';
            ignore_onend = true;
        }
        if (event.error == 'not-allowed') {
            ignore_onend = true;
        }
    };

    recognition.onend = function () {
        recognizing = false;
        if (ignore_onend) {
            return;
        }
        start_img.src = 'https://raw.githubusercontent.com/anooprh/PyOhio-Prsesentation/master/images/mic.gif';
        if (!final_transcript) {
            return;
        }
        if (window.getSelection) {
            window.getSelection().removeAllRanges();
            var range = document.createRange();
            range.selectNode(document.getElementById('final_span'));
            window.getSelection().addRange(range);
        }
    };

    recognition.onresult = function (event) {
        var interim_transcript = '';
        if (typeof(event.results) == 'undefined') {
            recognition.onend = null;
            recognition.stop();
            upgrade();
            console.log('No suitable phrase found');
            return;
        }
        for (var i = event.resultIndex; i < event.results.length; ++i) {
            if (event.results[i].isFinal) {
                final_transcript += event.results[i][0].transcript;
            } else {
                interim_transcript += event.results[i][0].transcript;
            }
        }
        final_transcript = capitalize(final_transcript);
        final_span.innerHTML = linebreak(final_transcript);
        interim_span.innerHTML = linebreak(interim_transcript);

        takeSpeechInputAndDoAction(final_transcript);
    };
}

function takeSpeechInputAndDoAction(speechInput){
	 var scores = [];

	 // var dmp = new diff_match_patch();
	 for(var i = 0 ; i < no_of_captions ; i++){
		 var score = findScore(speechInput, texts[i]);
		 scores.push(score);
	 }
     //console.log(scores);
     var lowest_score = Math.max.apply(Math, scores);
	 var lowestIndex = scores.indexOf(lowest_score);
     if(lowest_score >= 1){
	     moveToSpecifidTimeInVideo(times[lowestIndex]);
     }else{
         console.log("Not close enough match");
     }
}

function findScore(phrase, line){
	var line_words = line.toLowerCase().replace(/[^\w\s]|_/g, "").replace(/\s+/g, " ").split(' ');
	var phrase_words = phrase.toLowerCase().split(' ');
	var score = 0;
	for (var i = 0 ; i < line_words.length; i++){
		for (var j = 0 ; j < phrase_words.length ; j++){
            if(line_words[i] == "")continue;
			if(line_words[i] == phrase_words[j]) score++;
		}
	}
	return score;

}

function moveToSpecifidTimeInVideo(time){
	// Start at seven seconds before actual time frame 
	bufferTime = 5;
    console.log(time);
	splits = time.split(':');
	minutes = parseInt(splits[0]);
	seconds = parseInt(splits[1]);
    console.log(minutes, seconds);
	seekTime = minutes*60 + seconds - bufferTime;
    console.log(seekTime);
	var video = document.getElementsByTagName("video")[0];
	video.currentTime = seekTime;

	// setTimeout(watchheader.removeChild(splitseechbox), 1000);
}

function upgrade() {
    start_button.style.visibility = 'hidden';
    showInfo('info_upgrade');
}

var two_line = /\n\n/g;
var one_line = /\n/g;
function linebreak(s) {
    return s.replace(two_line, '<p></p>').replace(one_line, '<br>');
}

var first_char = /\S/;
function capitalize(s) {
    return s.replace(first_char, function (m) {
        return m.toUpperCase();
    });
}

if (recognizing) {
    recognition.stop();
    return;
}
final_transcript = '';
recognition.lang = 'en_US';
recognition.start();
ignore_onend = false;
final_span.innerHTML = '';
interim_span.innerHTML = '';
start_img.src = 'https://raw.githubusercontent.com/anooprh/PyOhio-Prsesentation/master/images/mic-slash.gif';
